from math import factorial
from typing import Iterator, List, TypeVar
import turtle


def besie(P: List[List[float]], count: int) -> Iterator[List[float]]:
    n = len(P) - 1
    m = len(P[0])

    def b(k: int, n: int, t: float) -> float:
        binomial_k = factorial(n) / (factorial(k) * factorial(n - k))
        return binomial_k * (t ** k) * ((1 - t) ** (n - k))

    def z(j: int, t: float) -> float:
        return sum(map(
            lambda k: P[k][j - 1] * b(k, n, t),
            range(n + 1)
        ))

    for t in range(count + 1):
        t /= count
        r = []
        for i in range(1, m + 1):
            r.append(z(i, t))
        yield r


turtle.hideturtle()
P = [
    [0, 0],
    [200, 500],
    [1000, 250],
    [500, 100]
]
for i in P:
    turtle.goto(*i)
turtle.color('white')
turtle.goto(0, 0)
turtle.color('red')
for i in besie(P, 1000):
    turtle.goto(*i)

input()
